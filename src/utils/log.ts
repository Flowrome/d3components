import { environment } from 'src/environments/environment';

export const logLabel = {
  log: 'Logging:',
  warn: 'Warning:',
  error: 'Error',
};

export const logType = {
  log: 'log',
  warn: 'warn',
  error: 'error',
};

export const logStyle = {
  error: `color: red; font-size: 2em`,
  warn: `color: orange; font-size: 1em`,
  log: `color: green; font-size: 1em`
};

export function clog(log: any | any[], type: string = logType.log) {
  if (environment.production) {
    return;
  }
  console.group(`%c${logLabel[type]}`, logStyle[type]);
  if (Array.isArray(log)) {
    log.map(logging => {
      console.log(logging);
    });
  } else {
    console.log(log);
  }
  console.groupEnd();
}

export function cerror(log: any) {
  if (environment.production) {
    return;
  }
  console.group(`%c${logLabel[logType.error]}`, logStyle[logType.error]);
  console.log(`${log}`);
  console.groupEnd();
}

export function cwarn(log: any) {
  if (environment.production) {
    return;
  }
  console.group(`%c${logLabel[logType.warn]}`, logStyle[logType.warn]);
  console.log(`${log}`);
  console.groupEnd();
}
