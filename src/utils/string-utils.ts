import * as moment from 'moment';
import { defaultDateFormat } from 'src/const';

export function generateUUID(): string {
  let date = new Date().getTime();
  if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
    date += performance.now();
  }
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    // tslint:disable-next-line:no-bitwise
    const random = (date + Math.random() * 16) % 16 | 0;
    date = Math.floor(date / 16);
    // tslint:disable-next-line:no-bitwise
    return (c === 'x' ? random : (random & 0x3 | 0x8)).toString(16);
  });
}

export function makeHTML(text: string, htmlTAG: string, classes?: string): string {
  return (text) ? `<${htmlTAG} class="${classes}">${text}</${htmlTAG}>` : '';
}

export function checkIfDate(value: any, dateFormat: string = defaultDateFormat): boolean {
  return value && (typeof value === 'string') && (moment(value, dateFormat).isValid());
}

export function dateToTimestamp(value: string, dateFormat: string = defaultDateFormat): number {
  return value && moment(value, dateFormat).unix();
}

export function timestampToDate(value: number, dateFormat: string = defaultDateFormat): string {
  return moment.unix(value).format(dateFormat);
}
