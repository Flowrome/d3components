export {
  generateUUID,
  makeHTML,
  checkIfDate,
  dateToTimestamp,
  timestampToDate,
} from './string-utils';

export {
  waitNext,
  addPeriodToTimestamp,
  drawDiv,
  downloadObjectAsJson,
  scrollToElement,
  getPercentage,
  debounce,
  subPeriodToTimestamp
} from './functions';

export {
  clog,
  cerror,
  cwarn,
  logStyle,
  logType,
  logLabel
} from './log';
