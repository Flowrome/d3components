import * as moment from 'moment';
import { dateToTimestamp, timestampToDate } from './string-utils';
import { defaultDateFormat } from 'src/const';
import { clog } from './log';

export function waitNext(fn: Function) {
  setTimeout(() => {
    fn();
  }, 0);
}

export function addPeriodToTimestamp(timestamp: number, many: number, period: string, dateFormat: string = defaultDateFormat): number {
  const unit: moment.unitOfTime.DurationConstructor = (<any>period);
  const date = dateToTimestamp(moment(timestampToDate(timestamp, dateFormat), dateFormat)
    .add(many, unit).format(dateFormat), dateFormat);
  return date;
}

export function subPeriodToTimestamp(timestamp: number, many: number, period: string, dateFormat: string = defaultDateFormat): number {
  const unit: moment.unitOfTime.DurationConstructor = (<any>period);
  const date = dateToTimestamp(moment(timestampToDate(timestamp, dateFormat), dateFormat)
    .subtract(many, unit).format(dateFormat), dateFormat);
  return date;
}


export function drawDiv(
  container,
  axes: { x?: any, y?: any },
  classAttr: string,
  coords: { x: number, y: number },
  innerCoords?: { x: number, y: number },
  noAxes?: boolean
): any {

  const tmp = container.append('div')
    .attr('class', classAttr);

  waitNext(() => {
    const nodeHalfHeight = tmp.node().getBoundingClientRect().height / 2;
    const nodeHalfWidth = tmp.node().getBoundingClientRect().width / 2;

    let top = ((noAxes) ? coords.y : axes.y(coords.y)) - nodeHalfHeight;
    let left = ((noAxes) ? coords.x : axes.x(coords.x)) - nodeHalfWidth;

    if (innerCoords && (innerCoords.x || innerCoords.y)) {
      top = (((noAxes) ? coords.y : axes.y(coords.y)) - nodeHalfHeight) +
        (nodeHalfHeight * (innerCoords.y / 100));
      left = (((noAxes) ? coords.x : axes.x(coords.x)) - nodeHalfWidth) +
        (nodeHalfWidth * (innerCoords.x / 100));
    }

    tmp
      .style('top', `${top}px`)
      .style('left', `${left}px`);
  });

  return tmp;
}

export function downloadObjectAsJson(exportObj: any, exportName: string = `chart-json-${moment().unix()}`) {
  const dataStr = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(exportObj));
  const downloadAnchorNode = document.createElement('a');
  downloadAnchorNode.setAttribute('href', dataStr);
  downloadAnchorNode.setAttribute('download', exportName + '.json');
  document.body.appendChild(downloadAnchorNode); // required for firefox
  downloadAnchorNode.click();
  downloadAnchorNode.remove();
}

export function scrollToElement(id: string, father?: string): void {
  let fatherElem: any = document;
  if (father) {
    fatherElem = document.querySelectorAll(`#${father}`)[0];
  }
  const element = fatherElem.querySelectorAll(`#${id}`)[0];
  if (element) {
    element.classList.add('selected');
    setTimeout(() => {
      element.classList.remove('selected');
    }, 1200);
    element.scrollIntoView({ behavior: 'smooth' });
  }
}

export function getPercentage(value: number, max: number): number {
  return value * 100 / max;
}

export function debounce(timer: any, timeout: number, fn: Function) {
  if (timer) {
    clearTimeout(timer);
  }
  timer = setTimeout(() => {
    fn();
  }, timeout);
}
