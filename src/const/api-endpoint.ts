import { environment } from 'src/environments/environment';
import * as moment from 'moment';
import { periodFromNow } from './vars';

const baseUrl = (environment.production) ? '' : '../assets/mock/api/';

export const apiEndpoint = {
  graphs: {
    presentation: `${baseUrl}graphs/graph-presentation.json`,
    presentation_diff: `${baseUrl}graphs/graph-presentation.json`,
    lineChart: `${baseUrl}graphs/graph-line-chart.json`,
    lineChartDates: `${baseUrl}graphs/graph-line-chart-dates.json`,
    barChart: `${baseUrl}graphs/graph-line-chart.json`,
    barChartDates: `${baseUrl}graphs/graph-line-chart-dates.json`,
    pieChart: `${baseUrl}graphs/graph-pie-chart.json`,
    presentation_gr_canvas_vs_svg: `${baseUrl}graphs/presentation/chart-json-presentation[canvas vs svgs].json`,
    presentation_gr_steps: `${baseUrl}graphs/presentation/chart-presentation[steps].json`,
    presentation_gr_what_is_d3: `${baseUrl}graphs/presentation/chart-presentation[what-is-d3].json`,
    presentation_gr_d3_hc_cjs: `${baseUrl}graphs/presentation/chart-json-presentation[D3vsHCvsCJS].json`,
    npmTrends: (
      library: string,
      period: string = periodFromNow(1, 'month')
    ) => `https://api.npmjs.org/downloads/range/${period}/${library}`
  }
};
