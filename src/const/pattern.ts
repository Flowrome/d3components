
// tslint:disable:max-line-length
export const patterns = {
  URL: /^(f|ht)tps?:\/\//i,
  EMAIL: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  AT_LEAST_ONE: {
    NUMBER: /(?=.*\d).*$/,
    SYMBOL: /(?=.*[$-/:-?{-~!"^_`\[\]]).*$/,
    LOW_CHAR: /(?=.*[a-z]).*$/,
    UPPER_CHAR: /(?=.*[A-Z]).*$/,
    NO_WHITE_SPACE: /^\S*$/
  },
  ONLY: {
    NUMBERS_INT: /^[0-9]+$/,
    NUMBERS_FLOAT: /^[+-]?([0-9]*[.,])?[0-9]+$/,
    LETTERS: /^[a-zA-Z]+$/,
  },
  COLOR: /^#(?:[0-9a-fA-F]{3}){1,2}$/,
  MATCH_SOMETHING_BETWEEN: (openChar: string, closingChar: string) => new RegExp(`[^${'\\'}${openChar}${'\\'}${closingChar}]+(?=${'\\'}${closingChar})`, 'g'),
  MIN_MAX_CHAR: (char: string, min: string|number = '0', max: string|number = '') => new RegExp(`^([^$${'\\'}${char}]*${'\\'}${char}){${min},${max}}[^${'\\'}${char}]*$`),
  MIN_MAX_LENGHT: (min = 0, max = Number.MAX_SAFE_INTEGER) => new RegExp(`^.{${min},${max}}$`)
};
