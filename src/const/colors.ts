export const colors = {
  default_light: {
    background: '#fafafa',
    background_dark: '#f5f5f5',
    background_light: '#ffffff',
    generals: {
      accent: '#74D3AE',
      accent_light: '#7BE0B9',
      accent_dark: '#6BC4A2',
      primary: '#0B3142',
      primary_light: '#0D3C51',
      primary_dark: '#071F2B',
    },
    infos: {
      error: '#ef233c',
      warning: '#fa9f42',
      important: '#2de1fc',
      success: '#cfee9e',
      standard: '#46444d'
    }
  }
};

