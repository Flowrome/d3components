import * as moment from 'moment';

export const defaultDateFormat = 'DD/MM/YYYY';
export const defaultTimeFormat = 'HH:MM:SS';

export const lineTypes = {
  curveLinear: 'curveLinear',
  curveNatural: 'curveNatural',
  curveCardinalClosed: 'curveCardinalClosed',
  curveCardinalOpen: 'curveCardinalOpen',
  curveCardinal: 'curveCardinal',
  curveBundle: 'curveBundle',
  curveBasisClosed: 'curveBasisClosed',
  curveBasisOpen: 'curveBasisOpen',
  curveBasis: 'curveBasis',
  curveStepAfter: 'curveStepAfter',
  curveStepBefore: 'curveStepBefore'
};

export const periodFromNow = (many: number = 1, period: string = 'month') =>
// tslint:disable-next-line: deprecation
  `${moment().subtract((<any> many), period).format('YYYY-MM-DD')}:${moment().subtract(1, 'day').format('YYYY-MM-DD')}`;
