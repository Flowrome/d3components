import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { apiEndpoint, defaultDateFormat, lineTypes } from 'src/const';

import * as moment from 'moment';
import { ChartLine, PieChart, PieSegments } from 'src/interfaces';
import { clog } from 'src/utils';
import { map } from 'rxjs/operators';
import { BarSegment } from 'src/interfaces/graphs';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  getGraphData(path: string): Observable<any> {
    return this.http.get(apiEndpoint.graphs[path]);
  }

  getNPMTrendData(library: string, color: string, method: string, period?: string): Observable<any> {
    return this.http.get(apiEndpoint.graphs.npmTrends(library, period))
      .pipe(
        map((data: any) => {
          const toReturn = this[method](data, color);
          return toReturn;
        })
      );
  }

  private parseForLineChart(data, color): ChartLine {
    const toReturn: ChartLine = {
      type: lineTypes.curveNatural,
      name: data.package,
      color: color,
      coords: [...data.downloads.map(download => ({
        x: moment(download.day, 'YYYY-MM-DD')
          .format(defaultDateFormat),
        xLabel: 'Date',
        y: download.downloads,
        yLabel: 'Downloads',
      }))]
    };

    return toReturn;
  }

  private parseForPieChart(data, color): PieSegments {
    const toReturn: PieSegments = {
      value: data.downloads.reduce((a, b) => a + b.downloads, 0),
      color: color,
      description: `From: ${moment(data.start).format('DD/MM/YY')}, To: ${moment(data.end).format('DD/MM/YY')}`,
      title: data.package
    };

    return toReturn;
  }

  private parseForBarChartSingleValue(data, color): BarSegment[] {
    const toReturn: BarSegment[] = data.downloads.map(stats => {
      return {
        coords: {
          xLabel: 'Date',
          x: moment(stats.day).format('DD/MM/YY'),
          yLabel: 'Downloads',
          y: stats.downloads
        },
        color: color,
        name: data.package,
        description: `${moment(data.end).format('DD/MM/YY')}`,
      };
    });
    return toReturn;
  }

  private parseForBarChartGroupValue(data, color): BarSegment {
    const toReturn: BarSegment = {
      coords: {
        xLabel: 'Period',
        x: data.package,
        yLabel: 'Downloads',
        y: data.downloads.reduce((a, b) => a + b.downloads, 0),
      },
      color: color,
      name: data.package,
      description: `From: ${moment(data.start).format('DD/MM/YY')}, To: ${moment(data.end).format('DD/MM/YY')}`,
    };

    return toReturn;
  }
}
