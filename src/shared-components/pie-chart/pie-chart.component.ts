import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { colors } from 'src/const';

import * as d3 from 'd3';
import { environment } from 'src/environments/environment';
import { PieChart, PieSegments } from 'src/interfaces';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/services';
import { Observable, of, zip } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { generateUUID, clog, getPercentage, drawDiv, waitNext, debounce, makeHTML } from 'src/utils';
@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html'
})
export class PieChartComponent implements OnInit, AfterViewInit {

  @Input() pieChartData: PieChart;
  @Input() noAnimation: boolean;
  @Output() clickOnCard: EventEmitter<PieSegments>;

  id: string;
  _id: string;

  private width: number;
  private height: number;
  private generalContainer;
  private margins: any;
  private svgContainer;
  private pathContainer;

  private pieDraw: {
    duration: number,
    delayTimeout?: number,
    color: string,
    width: number,
    outerArcRadiusesMulti: {
      outer: number,
      inner: number,
    },
    arcRadiusesMulti: {
      outer: number,
      inner: number,
    },
    tooltipDuration: number,
    divRadiusesMulti: number,
    padAngle: number,
    cornerRadius: number,
  };

  private radius: number;
  private pie: any;
  private arc: any;
  private arcZero: any;
  private outerArc: any;
  private maxValue: number;
  private tooltipTimeout;

  private currentSelectionId: string;
  private currentSelectionValue;

  constructor(
    private api: ApiService,
    private route: ActivatedRoute
  ) {
    this.id = `pie-chart-${generateUUID()}`;
    this._id = `#${this.id}`;
    this.pieDraw = {
      duration: 1000,
      tooltipDuration: 1000,
      delayTimeout: 500,
      outerArcRadiusesMulti: {
        outer: 0.9,
        inner: 0.9,
      },
      arcRadiusesMulti: {
        outer: 0.8,
        inner: 0.6,
      },
      divRadiusesMulti: 0.5,
      padAngle: 0.015,
      cornerRadius: 3,
      color: colors.default_light.generals.accent,
      width: 2
    };
  }

  ngOnInit() {
    this.pieDraw = {
      ...this.pieDraw,
      duration: (this.noAnimation) ? 0 : 2000,
      delayTimeout: (this.noAnimation) ? 0 : 1000,
    };
  }

  ngAfterViewInit(): void {
    this.initRequestData().subscribe(() => {
      this.initGraph();
    });
  }

  reDraw(): void {
    const container = d3.select(this._id);
    container.selectAll('*').remove();
    this.pieDraw = {
      ...this.pieDraw,
      duration: 0,
      delayTimeout: 0
    };
    this.initGraph();
  }

  private initGraph(): void {
    this.maxValue = this.pieChartData.segments.reduce((accumulator, currentValue) => accumulator + currentValue.value, 0);
    this.initChart();
    this.initPie();
    this.drawSegments();
  }

  private initRequestData(): Observable<any> {
    if (
      !environment.production &&
      !this.pieChartData &&
      ((this.route.snapshot || (<any>{})).data || {}).pieChartPath
    ) {
      return zip(
        this.api.getNPMTrendData('d3', '#586BA4', 'parseForPieChart'),
        this.api.getNPMTrendData('chart.js', '#7B9E89', 'parseForPieChart'),
        this.api.getNPMTrendData('highcharts', '#F68E5F', 'parseForPieChart')
      )
      .pipe(
        map((data: PieSegments[]) => {
          this.pieChartData = { ...data, segments: data.sort((a, b) => a.value - b.value) };
        })
      );
      // return this.api.getGraphData(this.route.snapshot.data.pieChartPath).pipe(
      //   take(1),
      //   map((data: PieChart) => {
      //     this.pieChartData = { ...data, segments: data.segments.sort((a, b) => a.value - b.value) };
      //   })
      // );
    }
    return of();
  }

  private initChart(): void {
    const container = d3.select(this._id);
    this.generalContainer = container;
    this.width = container.node().getBoundingClientRect().width || 0;
    this.height = container.node().getBoundingClientRect().height || 0;

    this.svgContainer = container.append('svg')
      .attr('width', this.width)
      .attr('height', this.height);

    this.pathContainer = this.svgContainer.append('g')
      .attr('transform', `translate( ${this.width / 2}, ${this.height / 2})`);

    this.radius = Math.min(this.height / 1.5, this.width / 1.5) / 1.5;
  }

  private initPie(): void {
    this.pie = d3.pie()
      .value((d) => {
        return d.value;
      })
      .sortValues((a, b) => a - b)(this.pieChartData.segments.map(seg => {
        seg.percentage = `${getPercentage(seg.value, this.maxValue).toFixed(2)}%`;
        return seg;
      }));

    this.outerArc = d3.arc()
      .outerRadius(this.radius * this.pieDraw.outerArcRadiusesMulti.outer)
      .innerRadius(this.radius * this.pieDraw.outerArcRadiusesMulti.inner);

    this.arc = d3.arc()
      .outerRadius(this.radius * this.pieDraw.arcRadiusesMulti.outer)
      .innerRadius((this.pieChartData.fullCircle) ? 0 : this.radius * this.pieDraw.arcRadiusesMulti.inner)
      .cornerRadius((this.pieChartData.fullCircle) ? 0 : this.pieDraw.cornerRadius)
      .padAngle((this.pieChartData.fullCircle) ? 0 : this.pieDraw.padAngle);
    this.arcZero = d3.arc()
      .outerRadius(this.radius * this.pieDraw.arcRadiusesMulti.outer)
      .innerRadius((!this.pieChartData.fullCircle) ? 0 : this.radius * this.pieDraw.arcRadiusesMulti.inner)
      .cornerRadius((this.pieChartData.fullCircle) ? 0 : this.pieDraw.cornerRadius)
      .padAngle((this.pieChartData.fullCircle) ? 0 : this.pieDraw.padAngle);
  }

  private drawSegments(): void {
    const tmpContainer = this.pathContainer
      .selectAll('path')
      .data(this.pie)
      .enter();
    const tmpSegment = tmpContainer
      .append('path')
      .attr('class', 'pie-path')
      .attr('id', (d, i) => `pie-path-${i}`)
      .attr('fill', (d) => this.pieChartData.standardColor || d.data.color || this.pieDraw.color)
      .attr('d', '')
      .style('cursor', 'pointer')
      .on('click', (d, i) => {
        this.toggleArc();
        this.toggleArc(`pie-path-${i}`, d);
        this.drawTooltip(d.data, this.pieChartData.standardColor || d.data.color || this.pieDraw.color);
      })
      .transition()
      .duration(this.pieDraw.duration / 2)
      .delay((d, i) => i * this.pieDraw.delayTimeout)
      .attrTween('d', (datum) => {
        const index = d3.interpolate(datum.startAngle, datum.endAngle);
        return (interpolation) => {
          datum.endAngle = index(interpolation);
          return this.arc(datum);
        };
      });
  }

  private drawTooltip(segment: PieSegments, color: string): void {
    waitNext(() => {
      if (this.tooltipTimeout) { clearTimeout(this.tooltipTimeout); }
      this.tooltipTimeout = setTimeout(() => {
        this.removeTooltip();
        this.toggleArc();
      }, this.pieDraw.tooltipDuration * 12);

      let tooltip = null;

      const innerHTML = `
      <div class="info-container jc-center">
      ${makeHTML(segment.title, 'div', `title title-${this.id}`)}
      ${makeHTML(segment.description, 'div', `description description-${this.id}`)}
      ${makeHTML(`percentage: ${segment.percentage}`, 'div', `description description-${this.id}`)}
      ${makeHTML(`value: ${segment.value.toString()}`, 'div', `description description-${this.id}`)}
      ${makeHTML('close', 'i', `close-button close-button-${this.id} material-icons`)}
      </div>`;

      if (!this.generalContainer.selectAll('.pie-tooltip').empty()) {
        tooltip = this.generalContainer
          .selectAll('.pie-tooltip')
          .html(innerHTML)
          .attr('class', 'pie-tooltip')
          .style('width', `${2 * this.radius * this.pieDraw.divRadiusesMulti}px`)
          .style('height', `${2 * this.radius * this.pieDraw.divRadiusesMulti}px`)
          .style('border-color', `${color}`);
      } else {
        tooltip = this.generalContainer
          .append('div')
          .html(innerHTML)
          .attr('class', 'pie-tooltip')
          .style('width', `${2 * this.radius * this.pieDraw.divRadiusesMulti}px`)
          .style('height', `${2 * this.radius * this.pieDraw.divRadiusesMulti}px`)
          .style('border-color', `${color}`)
          .style('opacity', 0)
          .style('transform', 'scale(0)');

        tooltip
          .transition()
          .duration(this.pieDraw.duration / 6)
          .style('opacity', 1)
          .style('transform', 'scale(1)');
      }
      tooltip
        .selectAll(`.close-button`)
        .on('click', () => {
          this.toggleArc();
          this.removeTooltip();
        });
    });
  }

  private removeTooltip(): void {
    this.generalContainer
      .selectAll('.pie-tooltip')
      .transition()
      .duration(this.pieDraw.tooltipDuration / 6)
      .style('opacity', 0)
      .style('transform', 'scale(0)')
      .remove();
  }

  private toggleArc(id?: string, value?) {
    if (this.currentSelectionId && this.currentSelectionValue && !id && !value) {
      const arc = this.arc(this.currentSelectionValue);
      this.pathContainer
        .select(`#${this.currentSelectionId}`)
        .attr('d', arc);
    } else if (id && value) {
      this.currentSelectionId = id;
      this.currentSelectionValue = value;
      const arc = this.arcZero(value);
      this.pathContainer
        .select(`#${id}`)
        .attr('d', arc);
    }
  }

}
