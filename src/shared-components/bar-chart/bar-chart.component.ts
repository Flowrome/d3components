import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as d3 from 'd3';
import { Observable, of, zip } from 'rxjs';
import { map } from 'rxjs/operators';
import { colors, periodFromNow } from 'src/const';
import { environment } from 'src/environments/environment';
import { BarChart, BarSegment, Coords } from 'src/interfaces';
import { ApiService } from 'src/services';
import {
  addPeriodToTimestamp,
  checkIfDate,
  dateToTimestamp,
  generateUUID,
  subPeriodToTimestamp,
  timestampToDate,
  makeHTML,
  drawDiv,
  clog
} from 'src/utils';


@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html'
})
export class BarChartComponent implements OnInit, AfterViewInit {

  @Input() barChartData: BarChart;
  @Input() noAnimation: boolean;
  @Output() clickOnCard: EventEmitter<BarSegment>;

  id: string;
  _id: string;

  private width: number;
  private height: number;
  private generalContainer;
  private margins: any;
  private svgContainer;
  private pathContainer;

  private isXDate: boolean;
  private isYDate: boolean;

  private isXString: boolean;
  private isYString: boolean;

  private barDraw: {
    duration: number,
    delayTimeout?: number,
    color: string,
    width: number,
    radius: number,
  };

  private axes: { x?: any, y?: any };

  constructor(
    private api: ApiService,
    private route: ActivatedRoute
  ) {
    this.id = `bar-chart-${generateUUID()}`;
    this._id = `#${this.id}`;
    this.axes = {};
    this.margins = {
      top: 32,
      bottom: 32,
      left: 32,
      right: 32
    };
    this.barDraw = {
      duration: 1000,
      delayTimeout: 500,
      color: colors.default_light.generals.accent,
      width: 32,
      radius: 16
    };
  }

  ngOnInit() {
    this.barDraw = {
      ...this.barDraw,
      duration: (this.noAnimation) ? 0 : 2000,
      delayTimeout: (this.noAnimation) ? 0 : 1000,
    };
  }

  ngAfterViewInit(): void {
    this.initRequestData().subscribe(() => {
      this.initGraph();
    });
  }

  reDraw(): void {
    const container = d3.select(this._id);
    container.selectAll('*').remove();
    this.barDraw = {
      ...this.barDraw,
      duration: 0,
      delayTimeout: 0
    };
    this.initGraph();
  }

  private initGraph(): void {
    this.initChart();
    this.initAxes();
    this.drawBars(this.barChartData);
  }

  private initRequestData(): Observable<any> {
    if (
      !environment.production &&
      !this.barChartData &&
      ((this.route.snapshot || (<any>{})).data || {}).barChartPath
    ) {
      return zip(
        // this.api.getNPMTrendData('d3', '#586BA4', 'parseForBarChartGroupValue'),
        // this.api.getNPMTrendData('chart.js', '#7B9E89', 'parseForBarChartGroupValue'),
        // this.api.getNPMTrendData('highcharts', '#F68E5F', 'parseForBarChartGroupValue')
        this.api.getNPMTrendData('d3', '#586BA4', 'parseForBarChartSingleValue', periodFromNow(1.5, 'week')),
        this.api.getNPMTrendData('chart.js', '#7B9E89', 'parseForBarChartSingleValue', periodFromNow(1.5, 'week')),
        this.api.getNPMTrendData('highcharts', '#F68E5F', 'parseForBarChartSingleValue', periodFromNow(1.5, 'week'))
      )
        .pipe(
          map((data: BarSegment[]) => {

            const dataTmp = (<any>data).flat();
            this.barChartData = {
              bars: dataTmp,
            };
          })
        );
      // return this.api.getGraphData(this.route.snapshot.data.lineChartPath).pipe(
      //   take(1),
      //   map((data: LineChart) => {
      //     this.barChartData = data;
      //   })
      // );
    }
    return of();
  }

  private initChart(): void {
    const container = d3.select(this._id);
    if (!container) {
      return;
    }
    this.generalContainer = container;
    this.width = container.node().getBoundingClientRect().width || 0;
    this.height = container.node().getBoundingClientRect().height || 0;

    this.svgContainer = container.append('svg')
      .attr('width', this.width)
      .attr('height', this.height);

    this.pathContainer = this.svgContainer.append('g');
  }


  private initAxes(): void {
    let isDateChart = false;
    let isGroupString = false;
    const coords = this.barChartData.bars
      .map(bar => {
        return bar.coords;
      })
      .map(coord => {
        isDateChart = isDateChart || this.checkIfDateCoords(coord);
        isGroupString = isGroupString || this.checkIfGroupString(coord);
        return coord;
      });
    const maxX = Math.max(...coords.map(coord => {
      this.isXString = this.isXString || typeof coord.x === 'string';
      this.isXDate = this.isXDate || checkIfDate(coord.x);
      return (checkIfDate(coord.x)) ? dateToTimestamp(coord.x) : coord.x;
    }));
    const maxY = Math.max(...coords.map(coord => {
      this.isYString = this.isYString || typeof coord.y === 'string';
      this.isYDate = this.isYDate || checkIfDate(coord.y);
      return (checkIfDate(coord.y)) ? dateToTimestamp(coord.y) : coord.y;
    }));
    let domainX = [];
    let domainY = [];
    if (isDateChart) {
      const redundanceMany = 1;
      const redundancePeriod = 'week';
      const redundancePeriodSub = 'day';
      if (this.isXDate) {
        const minX = subPeriodToTimestamp(
          Math.min(...coords.map(coord => (checkIfDate(coord.x)) ? dateToTimestamp(coord.x) : coord.x)),
          redundanceMany,
          redundancePeriodSub);
        const maxes = {
          x: addPeriodToTimestamp(maxX, redundanceMany, redundancePeriod),
          y: maxY
        };
        domainX = [minX, maxes.x];
        domainY = [0, maxes.y];
      } else if (this.isYDate) {
        const minY = subPeriodToTimestamp(
          Math.min(...coords.map(coord => (checkIfDate(coord.y)) ? dateToTimestamp(coord.y) : coord.y)),
          redundanceMany,
          redundancePeriodSub);
        const maxes = {
          x: maxX,
          y: addPeriodToTimestamp(maxY, redundanceMany, redundancePeriod)
        };
        domainX = [0, maxes.x];
        domainY = [minY, maxes.y];
      }
    } else {
      domainX = [0, maxX];
      domainY = [0, maxY];
    }

    if (isGroupString) {
      domainX = (this.isXString) ? [...coords.map(coord => coord.x)] : [0, maxX];
      domainY = (this.isYString) ? [...coords.map(coord => coord.y)] : [0, maxY];
    }

    this.axes.x = d3.scaleBand()
      .padding(0.5)
      .range([0, this.width - this.margins.left])
      .domain(domainX);
    this.axes.y = d3.scaleLinear()
      .range([this.height - this.margins.top, 0])
      .domain(domainY);
    this.drawAxes();
  }

  private drawAxes(): void {
    const xFn = (this.isXDate && !this.isXString) ?
      d3.axisBottom(this.axes.x)
        .tickFormat((data) => {
          return timestampToDate(data, 'DD/MM/YY');
        }) :
      (this.isXString) ?
        d3.axisBottom(this.axes.x) :
        d3.axisBottom(this.axes.x).tickFormat((d) => d3.format('.2s')(d));
    const yFn = (this.isYDate && !this.isYString) ?
      d3.axisLeft(this.axes.y)
        .tickFormat((data) => {
          return timestampToDate(data, 'DD/MM/YY');
        }) :
      (this.isYString) ?
        d3.axisLeft(this.axes.y) :
        d3.axisLeft(this.axes.y).tickFormat((d) => d3.format('.2s')(d));
    const x = this.pathContainer.append('g')
      .attr('id', `axes-x-${this.id}`)
      .call(xFn.ticks(4).tickSize(-this.height + this.margins.top))
      .attr('opacity', 0)
      .attr('transform', `translate(${this.margins.left / 2}, ${this.height - this.margins.top / 2})`);

    x.selectAll('.domain').remove();
    x
      .transition()
      .duration(this.barDraw.duration / 2)
      .attr('opacity', 1);
    const y = this.pathContainer.append('g')
      .attr('id', `axes-y-${this.id}`)
      .call(yFn.ticks(4).tickSize(-this.width + this.margins.left))
      .attr('opacity', 0)
      .attr('transform', `translate(${this.margins.left / 2}, ${this.margins.top / 2})`);
    y.selectAll('.domain').remove();
    y
      .transition()
      .duration(this.barDraw.duration / 2)
      .attr('opacity', 1);
  }

  drawBars(data: BarChart) {
    const bars = this.pathContainer.selectAll('.bar')
      .data(data.bars)
      .enter()
      .append('rect')
      .on('click', (d, index) => {
        this.drawTooltip({
          x: (this.isXString) ? d.description : d.coords.x,
          xLabel: d.coords.xLabel,
          y: (this.isYString) ? d.description : d.coords.y,
          yLabel: d.coords.yLabel,
        }, index, d.name, d.color, d3.event);
      })
      .attr('class', (d, index) => `bar bar${index}-${this.id}`)
      .attr('x', (d) => this.axes.x(d.coords.x) + (this.axes.x.bandwidth() - this.barDraw.width) / 2)
      .attr('width', this.barDraw.width)
      .attr('transform', `translate(${this.margins.left / 2}, -${this.margins.top / 2})`)
      .attr('y', this.height)
      .attr('height', 0)
      .attr('fill', (d) => d.color)
      .attr('rx', this.barDraw.radius)
      .attr('ry', this.barDraw.radius)

      .transition()
      .duration(this.barDraw.duration)
      .attr('y', (d) => this.axes.y(d.coords.y) + this.margins.top)
      .attr('height', (d) => this.height - this.axes.y(d.coords.y) - this.margins.top);
  }

  private drawTooltip(coords: Coords, index: number, barName: string, color, event): void {
    this.removeTooltip();
    const yPos = event.y;
    const xPos = event.x;
    const halfHeight = this.height / 2;
    const sixthWidth = this.width / 9;

    const innerHTML = `
      <div class=info-container ai-flex-start>
      ${makeHTML('close', 'i', `close-button close-button-${this.id} material-icons`)}
      ${makeHTML(`Bar: ${barName}`, 'div', `description x-label-${this.id}`)}
        ${makeHTML(`${this.barChartData.xLabel || coords.xLabel ||
      ((this.isXDate && !this.isXString) ? 'Date' : 'Value X')}: ${(this.isXDate && !this.isXString)
        ? timestampToDate(coords.x, 'DD/MM/YY') : coords.x}`, 'div', `description x-label-${this.id}`)}
      ${makeHTML(`${this.barChartData.yLabel || coords.yLabel ||
          ((this.isYDate && !this.isYString) ? 'Date' : 'Value Y')}: ${(this.isYDate && !this.isYString)
            ? timestampToDate(coords.y, 'DD/MM/YY') : coords.y}`, 'div', `description y-label-${this.id}`)}
      </div>`;


    const classes = `bar-tooltip bar-tooltip-${this.id}`;
    const tooltip = drawDiv(
      this.generalContainer,
      this.axes,
      classes,
      {y: yPos, x: xPos},
      {
        x: (xPos < sixthWidth || xPos > this.width - sixthWidth) ? (xPos > this.width - sixthWidth) ? -80 : 80 : 0,
        y: (yPos < halfHeight) ? 50 : 0,
      },
      true)
      .html(innerHTML)
      .style('transform', `translate(${this.margins.left / 2}px, 0px)`)
      .style('opacity', 0);

    tooltip
      .transition()
      .duration(this.barDraw.duration / 10)
      .style('border-color', `${color}`)
      .style('transform', `translate(${this.margins.left / 2}px, 0px)`)
      .style('opacity', 1);


    tooltip
      .selectAll(`.close-button`)
      .on('click', () => this.removeTooltip());
  }

  private removeTooltip(): void {
    this.generalContainer.selectAll('.bar-tooltip')
      .transition()
      .duration(this.barDraw.duration / 4)
      .style('transform', `translate(${this.margins.left / 2}px, ${(this.margins.top / 2) + 10}px)`)
      .style('opacity', 0)
      .remove();
  }

  private checkIfDateCoords(coords: Coords): boolean {
    const xIsDate = checkIfDate(coords.x);
    const yIsDate = checkIfDate(coords.y);
    return xIsDate || yIsDate;
  }

  private checkIfGroupString(coords: Coords): boolean {
    const xIsDate = typeof coords.x === 'string';
    const yIsDate = typeof coords.y === 'string';
    return xIsDate || yIsDate;
  }
}
