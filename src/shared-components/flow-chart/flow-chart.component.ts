import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { generateUUID, makeHTML, waitNext, drawDiv, scrollToElement } from 'src/utils';

import * as d3 from 'd3';
import shave from 'shave';
import * as uniq from 'lodash.uniq';

import { colors } from 'src/const';
import { ApiService } from 'src/services';
import { FlowChart, ChartElement, Coords } from 'src/interfaces';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { take, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-flow-chart',
  templateUrl: './flow-chart.component.html'
})
export class FlowChartComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() flowChartData: FlowChart;
  @Input() noAnimation: boolean;
  @Input() showAxes: boolean;
  @Output() clickOnCard: EventEmitter<ChartElement>;

  id: string;
  _id: string;

  private width: number;
  private height: number;
  private generalContainer;
  private svgContainer;
  private pathContainer;

  private axes: { x?: any, y?: any };

  private lineDraw: {
    duration: number,
    delayTimeout?: number,
    color: string
    width: number
  };

  constructor(
    private api: ApiService,
    private route: ActivatedRoute
  ) {
    this.id = `flow-chart-${generateUUID()}`;
    this._id = `#${this.id}`;
    this.axes = {};
    this.clickOnCard = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.initRequestData().subscribe(() => {
      this.initGraph();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ((<any>(changes || {}).flowChartData || {}).currentValue) {
      waitNext(() => {
        this.reDraw();
      });
    }
  }

  reDraw(): void {
    const container = d3.select(this._id);
    container.selectAll('*').remove();
    this.noAnimation = true;
    this.initGraph();
  }

  private initGraph(): void {
    this.lineDraw = {
      duration: (this.noAnimation) ? 0 : 1500,
      delayTimeout: (this.noAnimation) ? 0 : 750,
      color: this.flowChartData.lineColor || colors.default_light.generals.accent,
      width: this.flowChartData.lineWidth || 2
    };
    this.initChart();
    this.initAxes();
    this.drawLines(this.flowChartData.withConnectionDots);
    this.drawElements();
  }

  private initRequestData(): Observable<any> {
    if (
      !environment.production &&
      !this.flowChartData &&
      ((this.route.snapshot || (<any>{})).data || {}).flowChartPath
    ) {
      return this.api.getGraphData(this.route.snapshot.data.flowChartPath).pipe(
        take(1),
        map((data: FlowChart) => {
          this.flowChartData = data;
        })
      );
    }
    return of();
  }

  private initChart(): void {
    const container = d3.select(this._id);
    this.generalContainer = container;
    this.width = container.node().getBoundingClientRect().width || 0;
    this.height = container.node().getBoundingClientRect().height || 0;

    this.svgContainer = container.append('svg')
      .attr('width', this.width)
      .attr('height', this.height);

    this.pathContainer = this.svgContainer.append('g');
  }

  private initAxes(): void {
    this.axes.x = d3.scaleLinear()
      .range([0, this.width])
      .domain([0, 100]);
    this.axes.y = d3.scaleLinear()
      .range([0, this.height])
      .domain([0, 100]);
    if (this.showAxes) {
      this.drawAxes();
    }
  }


  private drawAxes(): void {
    const xFn = d3.axisBottom(this.axes.x);
    const yFn = d3.axisLeft(this.axes.y);
    const x = this.pathContainer.append('g')
      .attr('id', `axes-x-${this.id}`)
      .call(xFn.ticks(4).tickSize(2))
      .attr('opacity', 0)
      .transition()
      .duration(this.lineDraw.duration / 2)
      .attr('opacity', 1);
    const y = this.pathContainer.append('g')
      .attr('id', `axes-y-${this.id}`)
      .call(yFn.ticks(4).tickSize(2))
      .attr('opacity', 0)
      .transition()
      .duration(this.lineDraw.duration / 2)
      .attr('opacity', 1);
  }

  private drawLines(withConnectionDot: boolean = false): void {
    const linePath = d3.line()
      .x((d) => this.axes.x(d.x))
      .y((d) => this.axes.y(d.y));

    const mergedPath = this.flowChartData.lines.map((path, index) => {
      const line = this.pathContainer.append('path')
        .datum(path.coords)
        .attr('d', linePath);
      const d = line.attr('d');

      if (!this.flowChartData.notSingleLine) {
        line.remove();
      } else {
        const id = `line-unique-${index}-${this.id}`;
        line
          .attr('id', id)
          .on('click', () => {
            scrollToElement(path.formId);
          })
          .style('cursor', 'pointer')
          .attr('fill', 'none')
          .attr('stroke', this.lineDraw.color)
          .attr('stroke-width', this.lineDraw.width);
      }
      return d;
    });

    if (!this.flowChartData.notSingleLine) {
      const id = `line-unique-${this.id}`;

      const path = this.pathContainer.append('path')
        .attr('id', id)
        .attr('d', mergedPath.join())
        .attr('fill', 'none')
        .attr('stroke', this.lineDraw.color)
        .attr('stroke-width', this.lineDraw.width);

      const lenght = path.node().getTotalLength();

      path
        .attr('stroke-dasharray', `${lenght} ${lenght}`)
        .attr('stroke-dashoffset', lenght)
        .transition()
        .delay(this.lineDraw.delayTimeout)
        .duration(this.lineDraw.duration)
        .attr('stroke-dashoffset', 0);
    }

    if (withConnectionDot) {
      uniq(this.flowChartData.lines).map((line, index) => {
        const dotClass = `dot dot-${index}-${this.id}`;
        line.coords.map((coords) => {
          drawDiv(this.generalContainer, this.axes, dotClass, coords)
            .style('opacity', '0')
            .style('background-color', '#ffffff')
            .style('border-color', this.lineDraw.color)
            .on('click', () => {
              scrollToElement(coords.formId, line.formId);
            })
            .transition()
            .delay(this.lineDraw.delayTimeout * 2)
            .duration(this.lineDraw.duration / 2)
            .style('opacity', '1');
        });
      });
    }
  }

  private drawElements(): void {
    this.flowChartData.elements.map((element: ChartElement, index) => {
      switch (element.type) {
        case 'card':
          this.drawCards(element, index);
          break;
        case 'card-title':
          this.drawTitleCards(element, index);
          break;
      }
    });
  }

  private drawCards(element: ChartElement, index: number): void {
    const maxTitleLength = 60;
    const maxDescLength = (this.flowChartData.maxDescription === -1) ? 999999 : this.flowChartData.maxDescription || 0;
    if (element.title.length > maxTitleLength || element.description.length > maxDescLength) {
      element.hasMore = true;
    }
    const cardClass = `${element.type} ${element.type}-${index}-${this.id}`;
    const innerHTML = `
      <div class="info-container">
      ${makeHTML(element.title, 'div', `title title-${index}-${this.id}`)}
      ${makeHTML(element.description, 'div', `description description-${index}-${this.id}`)}
      ${(element.hasMore && this.flowChartData.showOverflowButton) ? makeHTML(this.flowChartData.overflowButtonText
      || 'see more', 'div', `see-info see-info-${index}-${this.id}`) : ''}
      </div>`;
    drawDiv(this.generalContainer, this.axes, cardClass, element.coords, element.innerCoords)
      .html(innerHTML)
      .on('click', () => {
        scrollToElement(element.formId);
        if (element.hasMore) {
          this.clickOnCard.emit(element);
        }
      })
      .style('opacity', '0')
      .style('cursor', 'pointer')
      .style('transform', 'translateY(10px)')
      .transition()
      .duration(this.lineDraw.duration / 2)
      .style('transform', 'translateY(0px)')
      .style('opacity', '1')
      .select('.description');
    shave(`.title-${index}-${this.id}`, maxTitleLength);
    shave(`.description-${index}-${this.id}`, maxDescLength);

  }

  private drawTitleCards(element: ChartElement, index: number): void {
    const cardClass = `${element.type} ${element.type}-${index}-${this.id}`;
    const innerHTML = `
      <div class="info-container">
      ${makeHTML(element.title, 'div', `title title-${index}-${this.id}`)}
      </div>`;
    drawDiv(this.generalContainer, this.axes, cardClass, element.coords, element.innerCoords)
      .html(innerHTML)
      .on('click', () => {
        scrollToElement(element.formId);
      })
      .style('opacity', '0')
      .style('cursor', 'pointer')
      .style('transform', 'translateY(10px)')
      .transition()
      .duration(this.lineDraw.duration / 2)
      .style('transform', 'translateY(0px)')
      .style('opacity', '1')
      .select('.description');
  }

}
