import { Component, OnInit, forwardRef, Input, Injector } from '@angular/core';
import { generateUUID } from 'src/utils';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, NgControl } from '@angular/forms';

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputTextComponent),
      multi: true
    }
  ],
})
export class InputTextComponent implements OnInit, ControlValueAccessor {

  @Input() type: string = 'text';
  @Input() label: string;

  ngControl: NgControl;
  id: string;
  errors: any;

  private commonValue: string | number;

  constructor(
    private injector: Injector
  ) {
    this.id = `input-text-${generateUUID()}`;
  }

  ngOnInit() {
    this.ngControl = this.injector.get(NgControl);
  }

  writeValue(event): void {
    if (!event && event !== 0) {
      event = '';
    }
    const value: string | number = (typeof event === 'string' || typeof event === 'number') ?
      event : (<HTMLInputElement>event.target).value;
    this.commonValue = value;
    this.onChange(value);
  }

  checkErrors(): boolean {
    const errors: string[] = this.ngControl.errors && Object.keys(this.ngControl.errors);
    this.errors = errors || [];
    return errors && errors.length > 0;
  }

  getLabel(): string {
    return (this.label) ?
      `${this.label}${((
        this.ngControl.errors || {}).required ||
        (this.ngControl.errors || {}).requiredTrue ||
        (this.ngControl.errors || {})[this.ngControl.name]) ? '*' : ''}` : null;
  }

  onChange = (_): void => { };
  onTouched = (): void => { };
  registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
  registerOnTouched(fn: () => void): void { this.onTouched = fn; }

  get value(): string | number {
    return this.commonValue;
  }

  set value(value: string | number) {
    if (value !== this.commonValue) {
      this.commonValue = value;
      this.onChange(value);
    }
  }

}
