import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { Observable, of, zip } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ChartElement, LineChart, Coords, ChartLine } from 'src/interfaces';
import { take, map } from 'rxjs/operators';
import { ApiService } from 'src/services';
import { ActivatedRoute } from '@angular/router';

import * as d3 from 'd3';
import * as moment from 'moment';
import {
  generateUUID,
  checkIfDate,
  dateToTimestamp,
  timestampToDate,
  addPeriodToTimestamp,
  drawDiv,
  waitNext,
  makeHTML,
  subPeriodToTimestamp
} from 'src/utils';
import { colors, lineTypes } from 'src/const';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html'
})
export class LineChartComponent implements OnInit, AfterViewInit {

  @Input() lineChartData: LineChart;
  @Input() noAnimation: boolean;
  @Output() clickOnCard: EventEmitter<ChartElement>;

  id: string;
  _id: string;

  private width: number;
  private height: number;
  private generalContainer;
  private margins: any;
  private svgContainer;
  private pathContainer;

  private isXDate: boolean;
  private isYDate: boolean;

  private lineDraw: {
    duration: number,
    delayTimeout?: number,
    color: string,
    width: number,
    type: string,
  };

  private axes: { x?: any, y?: any };

  constructor(
    private api: ApiService,
    private route: ActivatedRoute
  ) {
    this.id = `line-chart-${generateUUID()}`;
    this._id = `#${this.id}`;
    this.axes = {};
    this.margins = {
      top: 32,
      bottom: 32,
      left: 32,
      right: 32
    };
    this.lineDraw = {
      duration: 1000,
      delayTimeout: 500,
      color: colors.default_light.generals.accent,
      width: 2,
      type: lineTypes.curveLinear
    };
  }

  ngOnInit() {
    this.lineDraw = {
      ...this.lineDraw,
      duration: (this.noAnimation) ? 0 : 2000,
      delayTimeout: (this.noAnimation) ? 0 : 1000,
    };
  }

  ngAfterViewInit(): void {
    this.initRequestData().subscribe(() => {
      this.initGraph();
    });
  }

  reDraw(): void {
    const container = d3.select(this._id);
    container.selectAll('*').remove();
    this.lineDraw = {
      ...this.lineDraw,
      duration: 0,
      delayTimeout: 0
    };
    this.initGraph();
  }

  private initGraph(): void {
    this.initChart();
    this.initAxes();
    this.toggleLines();
  }

  private initRequestData(): Observable<any> {
    if (
      !environment.production &&
      !this.lineChartData &&
      ((this.route.snapshot || (<any>{})).data || {}).lineChartPath
    ) {
      return zip(
        this.api.getNPMTrendData('d3', '#586BA4', 'parseForLineChart'),
        this.api.getNPMTrendData('chart.js', '#7B9E89', 'parseForLineChart'),
        this.api.getNPMTrendData('highcharts', '#F68E5F', 'parseForLineChart')
      )
        .pipe(
          map((data: ChartLine[]) => {
            this.lineChartData = {
              withConnectionDots: false,
              hasX: true,
              hasY: true,
              lines: data
            };
          })
        );
      // return this.api.getGraphData(this.route.snapshot.data.lineChartPath).pipe(
      //   take(1),
      //   map((data: LineChart) => {
      //     this.lineChartData = data;
      //   })
      // );
    }
    return of();
  }

  private initChart(): void {
    const container = d3.select(this._id);
    this.generalContainer = container;
    this.width = container.node().getBoundingClientRect().width || 0;
    this.height = container.node().getBoundingClientRect().height || 0;

    this.svgContainer = container.append('svg')
      .on('click', () => this.removeTooltip())
      .attr('width', this.width)
      .attr('height', this.height);

    this.pathContainer = this.svgContainer.append('g');
  }

  private initAxes(): void {
    let isDateChart = false;
    const coords = this.lineChartData.lines
      .map(line => {
        return line.coords;
      })
      .reduce((firstCoordsArray, secondCoordsArray) => [...firstCoordsArray, ...secondCoordsArray], [])
      .map(coord => {
        isDateChart = isDateChart || this.checkIfDateCoords(coord);
        return coord;
      });
    const redundance = 10;
    const maxX = Math.max(...coords.map(coord => {
      this.isXDate = this.isXDate || checkIfDate(coord.x);
      return (checkIfDate(coord.x)) ? dateToTimestamp(coord.x) : coord.x;
    }));
    const maxY = Math.max(...coords.map(coord => {
      this.isYDate = this.isYDate || checkIfDate(coord.y);
      return (checkIfDate(coord.y)) ? dateToTimestamp(coord.y) : coord.y;
    }));
    let domainX = [];
    let domainY = [];
    if (isDateChart) {
      const redundanceMany = 1;
      const redundancePeriod = 'week';
      const redundancePeriodSub = 'day';
      if (this.isXDate) {
        const minX = subPeriodToTimestamp(
          Math.min(...coords.map(coord => (checkIfDate(coord.x)) ? dateToTimestamp(coord.x) : coord.x)),
          redundanceMany,
          redundancePeriodSub);
        const maxes = {
          x: addPeriodToTimestamp(maxX, redundanceMany, redundancePeriod),
          y: maxY + redundance
        };
        domainX = [minX, maxes.x];
        domainY = [0, maxes.y];
      } else if (this.isYDate) {
        const minY = subPeriodToTimestamp(
          Math.min(...coords.map(coord => (checkIfDate(coord.y)) ? dateToTimestamp(coord.y) : coord.y)),
          redundanceMany,
          redundancePeriodSub);
        const maxes = {
          x: maxX + redundance,
          y: addPeriodToTimestamp(maxY, redundanceMany, redundancePeriod)
        };
        domainX = [0, maxes.x];
        domainY = [minY, maxes.y];
      }
    } else {
      domainX = [0, maxX + redundance];
      domainY = [0, maxY + redundance];
    }
    this.axes.x = d3.scaleLinear()
      .range([0, this.width - this.margins.left])
      .domain(domainX);
    this.axes.y = d3.scaleLinear()
      .range([this.height - this.margins.top, 0])
      .domain(domainY);
    this.drawAxes();
  }

  private drawAxes(): void {
    const xFn = (this.isXDate) ?
      d3.axisBottom(this.axes.x)
        .tickFormat((data) => {
          return timestampToDate(data, 'DD/MM/YY');
        }) :
      d3.axisBottom(this.axes.x).tickFormat((d) => d3.format('.2s')(d));
    const yFn = (this.isYDate) ?
      d3.axisLeft(this.axes.y)
        .tickFormat((data) => {
          return timestampToDate(data, 'DD/MM/YY');
        }) :
      d3.axisLeft(this.axes.y).tickFormat((d) => d3.format('.2s')(d));
    const x = this.pathContainer.append('g')
      .attr('id', `axes-x-${this.id}`)
      .call(xFn.ticks(4).tickSize(-this.height + this.margins.top))
      .attr('opacity', 0)
      .attr('transform', `translate(${this.margins.left / 2}, ${this.height - this.margins.top / 2})`);

    x.selectAll('.domain').remove();
    x
      .transition()
      .duration(this.lineDraw.duration / 2)
      .attr('opacity', 1);
    const y = this.pathContainer.append('g')
      .attr('id', `axes-y-${this.id}`)
      .call(yFn.ticks(4).tickSize(-this.width + this.margins.left))
      .attr('opacity', 0)
      .attr('transform', `translate(${this.margins.left / 2}, ${this.margins.top / 2})`);
    y.selectAll('.domain').remove();
    y
      .transition()
      .duration(this.lineDraw.duration / 2)
      .attr('opacity', 1);

  }

  private toggleLines(name?: string): void {
    if (name) {
      if (!d3.selectAll(`.${name}`).empty()) {
        d3.selectAll(`.${name}`).remove();
        d3.selectAll(`.line-dot-${name}`).remove();
        return;
      } else {
        const line = this.lineChartData.lines.find(lineData => lineData.name === name);
        this.singleLineDraw(line);
      }
    } else {
      this.lineChartData.lines.map(line => {
        this.singleLineDraw(line);
      });
    }
  }

  private singleLineDraw(line: ChartLine): void {
    const classes = `${line.name} line line-${this.id}`;
    const linePath = d3.line()
      .curve(d3[line.type || this.lineDraw.type])
      .x((d) => this.axes.x((this.isXDate) ? dateToTimestamp(d.x) : d.x))
      .y((d) => this.axes.y((this.isYDate) ? dateToTimestamp(d.y) : d.y));

    const path = this.pathContainer.append('path')
      .datum(line.coords)
      .attr('class', classes)
      .attr('d', linePath)
      .attr('fill', 'none')
      .attr('stroke', line.color || this.lineDraw.color)
      .attr('stroke-width', line.width || this.lineDraw.width);

    const lenght = path.node().getTotalLength();

    path
      .attr('stroke-dasharray', `${lenght} ${lenght}`)
      .attr('stroke-dashoffset', lenght)
      .attr('transform', `translate(${this.margins.left / 2}, ${this.margins.top / 2})`)
      .transition()
      .delay(this.lineDraw.delayTimeout / 2)
      .duration(this.lineDraw.duration)
      .attr('stroke-dashoffset', 0);

    this.drawLineDots(line);
  }

  private drawLineDots(line: ChartLine): void {
    line.coords.map((coord, index) => {
      this.singleDotDraw(coord, line.name, index, line.coords.length, line.color);
    });
  }

  private singleDotDraw(coords: Coords, lineName: string, index: number, numbOfCoords: number, color: string): void {
    const classes = `line-dot line-dot-${lineName} line-dot-${index}-${lineName}-${this.id}`;
    coords = {
      x: (this.isXDate) ? dateToTimestamp(coords.x) : coords.x,
      xLabel: coords.xLabel,
      y: (this.isYDate) ? dateToTimestamp(coords.y) : coords.y,
      yLabel: coords.yLabel
    };
    const dot = drawDiv(
      this.generalContainer,
      this.axes,
      classes,
      coords
    )
      .style('transform', `translate(${this.margins.left / 2}px, ${(this.margins.top / 2) + this.height}px)`)
      .style('border-color', color)
      .style('opacity', 0)
      .on('click', () => {
        this.drawTooltip(coords, index, lineName, color);
      })
      .transition()
      .duration(this.lineDraw.duration / 2)
      .delay(this.lineDraw.delayTimeout / numbOfCoords * index)
      .style('transform', `translate(${this.margins.left / 2}px, ${(this.margins.top / 2)}px)`)
      .style('opacity', 1);
  }

  private drawTooltip(coords: Coords, index: number, lineName: string, color): void {
    this.removeTooltip();
    const yPos = this.axes.y(coords.y);
    const xPos = this.axes.x(coords.x);
    const halfHeight = this.height / 2;
    const sixthWidth = this.width / 9;

    const innerHTML = `
      <div class=info-container ai-flex-start>
      ${makeHTML('close', 'i', `close-button close-button-${this.id} material-icons`)}
      ${makeHTML(`Line: ${lineName}`, 'div', `description x-label-${this.id}`)}
        ${makeHTML(`${this.lineChartData.xLabel || coords.xLabel ||
      ((this.isXDate) ? 'Date' : 'Value X')}: ${(this.isXDate)
        ? timestampToDate(coords.x, 'DD/MM/YY') : coords.x}`, 'div', `description x-label-${this.id}`)}
      ${makeHTML(`${this.lineChartData.yLabel || coords.yLabel ||
          ((this.isYDate) ? 'Date' : 'Value Y')}: ${(this.isYDate)
            ? timestampToDate(coords.y, 'DD/MM/YY') : coords.y}`, 'div', `description y-label-${this.id}`)}
      </div>`;

    const classes = `line-tooltip line-tooltip-${this.id}`;
    const tooltip = drawDiv(
      this.generalContainer,
      this.axes,
      classes,
      coords,
      {
        x: (xPos < sixthWidth || xPos > this.width - sixthWidth) ? (xPos > this.width - sixthWidth) ? -80 : 80 : 0,
        y: (yPos < halfHeight) ? 150 : -100,
      })
      .html(innerHTML)
      .style('transform', `translate(${this.margins.left / 2}px, 0px)`)
      .style('opacity', 0);

    tooltip
      .transition()
      .duration(this.lineDraw.duration / 10)
      .style('border-color', `${color}`)
      .style('transform', `translate(${this.margins.left / 2}px, 0px)`)
      .style('opacity', 1);


    tooltip
      .selectAll(`.close-button`)
      .on('click', () => this.removeTooltip());
  }

  private removeTooltip(): void {
    this.generalContainer.selectAll('.line-tooltip')
      .transition()
      .duration(this.lineDraw.duration / 4)
      .style('transform', `translate(${this.margins.left / 2}px, ${(this.margins.top / 2) + 10}px)`)
      .style('opacity', 0)
      .remove();
  }

  private checkIfDateCoords(coords: Coords): boolean {
    const xIsDate = checkIfDate(coords.x);
    const yIsDate = checkIfDate(coords.y);
    return xIsDate || yIsDate;
  }

}
