import { FlowChart, ChartLine, ChartElement, Coords } from 'src/interfaces';
import { FormGroup, Validators, FormArray, FormBuilder } from '@angular/forms';
import { patterns, colors } from 'src/const';
import { generateUUID, waitNext, clog } from 'src/utils';
import { Observable, Subject } from 'rxjs';

export class FlowChartCreator {
  private _chart: FlowChart;
  private lineForms: FormGroup;
  private elementForms: FormGroup;
  private _settingForm: FormGroup;

  private isResetting: boolean;
  private _reset: Subject<void>;

  private defaultElem: ChartElement;
  private defaultLine: ChartLine;

  constructor(
    private formBuilder: FormBuilder
  ) {
    this._settingForm = this.formBuilder.group(this.settingsFieldCreator());
    this.lineForms = this.formBuilder.group({
      items: this.formBuilder.array([])
    });
    this.elementForms = this.formBuilder.group({
      items: this.formBuilder.array([])
    });

    this._reset = new Subject<void>();

    this.defaultElem = {
      coords: {
        x: 0,
        y: 0
      },
      type: this.enums.ELEMENT_TYPES[0].id,
      innerCoords: {
        x: 0,
        y: 0
      },
      description: '',
      title: ''
    };

    this.defaultLine = {
      coords: [
        {
          x: 0,
          y: 0
        }
      ],
      name: '',
      color: colors.default_light.generals.accent,
      width: 2
    };

    this.addElement();
    this.addLine();
  }

  get chart() {
    return this._chart;
  }

  get enums() {
    return {
      ELEMENT: 'elements',
      LINES: 'lines',
      ELEMENT_TYPES: [
        {
          label: 'Card',
          id: 'card'
        },
        {
          label: 'Title card',
          id: 'card-title'
        },
      ]
    };
  }

  get values() {
    return {
      elements: this.elementForms.getRawValue(),
      lines: this.lineForms.getRawValue(),
      settings: this.settingForm.getRawValue()
    };
  }

  get validity() {
    return {
      isValid: this.elementForms.valid && this.lineForms.valid && this.settingForm.valid,
      atLeastOne: this.elementForms.valid || this.lineForms.valid || this.settingForm.valid,
      elements: this.elementForms.valid,
      lines: this.lineForms.valid,
      settings: this.settingForm.valid
    };
  }

  get creator() {
    return this;
  }

  get elementsFormArray() {
    return this.elementForms;
  }

  get linesFormArray() {
    return this.lineForms;
  }

  get settingForm() {
    return this._settingForm;
  }

  get valueChanges(): { [key: string]: Observable<any> } {
    return {
      lines: this.lineForms.valueChanges,
      elements: this.lineForms.valueChanges,
    };
  }

  get reset(): Subject<void> {
    return this._reset;
  }

  get resetting(): boolean {
    return this.isResetting;
  }

  resetForm(nameForm?: string) {
    this.isResetting = true;
    switch (nameForm) {
      case '_settingForm':
        this._settingForm.reset();
        this._settingForm = this.formBuilder.group(this.settingsFieldCreator());
        break;
      case 'lineForms':
        this.lineForms.reset();
        this.lineForms = this.formBuilder.group({
          items: this.formBuilder.array([])
        });
        this.addLine();
        break;
      case 'elementForms':
        this.elementForms.reset();
        this.elementForms = this.formBuilder.group({
          items: this.formBuilder.array([])
        });
        this.addElement();
        break;
      default:
        this._settingForm.reset();
        this.lineForms.reset();
        this.elementForms.reset();
        this._settingForm = this.formBuilder.group(this.settingsFieldCreator());
        this.lineForms = this.formBuilder.group({
          items: this.formBuilder.array([])
        });
        this.addLine();
        this.elementForms = this.formBuilder.group({
          items: this.formBuilder.array([])
        });
        this.addElement();
        break;
    }
    this._reset.next();
    waitNext(() => {
      this.isResetting = false;
    });
    return this;
  }

  addElement() {
    (<FormArray>this.elementForms.get('items')).push(this.formBuilder.group(this.elementFieldsCreator()));
    return this;
  }

  removeElement(index: number) {
    (<FormArray>this.elementForms.get('items')).removeAt(index);
    return this;
  }

  addLine() {
    (<FormArray>this.lineForms.get('items')).push(this.formBuilder.group(this.lineFieldsCreator()));
    return this;
  }

  removeLine(index: number) {
    (<FormArray>this.lineForms.get('items')).removeAt(index);
    return this;
  }

  addPoint(control: any) {
    control.push(this.formBuilder.group(this.lineCoordsFieldsCreator()));
    return this;
  }

  removePoint(control: any, indexPoint: number) {
    control.removeAt(indexPoint);
    return this;
  }

  addElementPoint(control: any, index: number) {
    control.at(index).controls.elements.push(
      this.formBuilder.group(this.elementFieldsCreator(
        {
          ...this.defaultElem,
          coords: {
            x: control.at(index).controls.x.value,
            y: control.at(index).controls.y.value
          }
        },
        true)
      ));
    return this;
  }

  removeElementPoint(control: any, indexPoint: number) {
    control.removeAt(indexPoint);
    return this;
  }

  rebuildFromJSON(json: FlowChart): void {
    this.resetForm();
    (<FormArray>this.elementForms.get('items')).removeAt(0);
    json.elements.map(elem => {
      (<FormArray>this.elementForms.get('items'))
        .push(this.formBuilder.group(this.elementFieldsCreator({
          ...this.defaultElem,
          ...elem
        })));
    });
    (<FormArray>this.lineForms.get('items')).removeAt(0);
    json.lines.map(line => {
      (<FormArray>this.lineForms.get('items'))
        .push(this.formBuilder.group(this.lineFieldsCreator({
          ...this.defaultLine,
          ...line
        })));
    });
    this._settingForm.patchValue({
      ...{ color: colors.default_light.generals.accent, width: 2, withConnectionDots: false },
      ...{ color: json.lineColor, width: json.lineWidth, withConnectionDots: json.withConnectionDots }
    });
  }

  buildChart(isCreating?: boolean): FlowChart {
    if (this.validity.atLeastOne) {
      const chart: FlowChart = {
        ...((this.validity.settings) ? {
          withConnectionDots: this.values.settings.withConnectionDots,
          lineColor: this.values.settings.color,
          lineWidth: this.values.settings.width,
          maxDescription: this.values.settings.maxDescription,
          overflowButtonText: this.values.settings.overflowButtonText,
          showOverflowButton: this.values.settings.showOverflowButton,
          notSingleLine: isCreating
        } : {}),
        lines: (this.validity.lines) ? this.values.lines.items.map((line, index) => {
          const tmpLine: ChartLine = {
            name: line.name,
            id: `line-flow-chart-${generateUUID()}`,
            formId: `line-${index + 1}`,
            coords: line.points.map((coord, index2) => ({
              formId: `coord-${index2 + 1}`,
              x: parseFloat(coord.x),
              y: parseFloat(coord.y)
            })),
            width: parseInt(line.width, 10)
          };
          return tmpLine;
        }) : [],
        elements: [
          ...((this.validity.elements) ? this.values.elements.items.map((element, index) => {
            const tmpElem: ChartElement = {
              id: `element-flow-chart-${generateUUID()}`,
              formId: `element-${index + 1}`,
              description: element.description,
              title: element.title,
              type: element.type,
              coords: {
                x: parseFloat(element.x),
                y: parseFloat(element.y),
              },
              innerCoords: {
                x: parseFloat(element.inner_x),
                y: parseFloat(element.inner_y),
              }
            };
            return tmpElem;
          }) : []),
          ...((this.validity.lines) ? this.values.lines.items
            .map((line, index) => line.points.map((point, index2) => ({...point, lineFormId: `line-${index + 1}-coord-${index2 + 1}-`})))
            .reduce((firstPointsArray, secondPointsArray) => [...firstPointsArray, ...secondPointsArray], [])
            .map((points) => points.elements.map(elem => ({...elem, coordFormId: `${points.lineFormId}`})))
            .reduce((firstElArray, secondElArray) => [...firstElArray, ...secondElArray], [])
            .map((element) => {
              const tmpElem: ChartElement = {
                id: `element-flow-chart-${generateUUID()}`,
                formId: `${element.coordFormId}sub-element-1`,
                description: element.description,
                title: element.title,
                type: element.type,
                coords: {
                  x: parseFloat(element.x),
                  y: parseFloat(element.y),
                },
                innerCoords: {
                  x: parseFloat(element.inner_x),
                  y: parseFloat(element.inner_y),
                }
              };
              return tmpElem;
            }) : [])
        ],
      };
      return chart;
    }
  }

  private elementFieldsCreator(object: ChartElement = this.defaultElem, disabled: boolean = false) {
    return {
      type: this.formBuilder.control(object.type),
      x: this.formBuilder.control({ value: object.coords.x, disabled: disabled }, [
        Validators.max(100),
        Validators.min(0),
        Validators.pattern(patterns.ONLY.NUMBERS_FLOAT)
      ]),
      y: this.formBuilder.control({ value: object.coords.y, disabled: disabled }, [
        Validators.max(100),
        Validators.min(0),
        Validators.pattern(patterns.ONLY.NUMBERS_FLOAT)
      ]),
      inner_x: this.formBuilder.control(object.innerCoords.x, [
        Validators.max(100),
        Validators.min(-100),
        Validators.pattern(patterns.ONLY.NUMBERS_FLOAT)
      ]),
      inner_y: this.formBuilder.control(object.innerCoords.y, [
        Validators.max(100),
        Validators.min(-100),
        Validators.pattern(patterns.ONLY.NUMBERS_FLOAT)
      ]),
      description: this.formBuilder.control(object.description, [
        Validators.maxLength(300)
      ]),
      title: this.formBuilder.control(object.title, [
        Validators.maxLength(60)
      ])
    };
  }

  private lineCoordsFieldsCreator(object: Coords = { x: 0, y: 0 }) {
    return {
      x: this.formBuilder.control(object.x, [
        Validators.max(100),
        Validators.min(0),
        Validators.pattern(patterns.ONLY.NUMBERS_FLOAT)
      ]),
      y: this.formBuilder.control(object.y, [
        Validators.max(100),
        Validators.min(0),
        Validators.pattern(patterns.ONLY.NUMBERS_FLOAT)
      ]),
      elements: this.formBuilder.array([])
    };
  }

  private lineFieldsCreator(object: ChartLine = this.defaultLine) {
    return {
      name: this.formBuilder.control(object.name, [
        Validators.maxLength(60)
      ]),
      points: this.formBuilder.array([
        ...object.coords.map(coord => {
          return this.formBuilder.group(this.lineCoordsFieldsCreator(coord));
        })
      ])
    };
  }

  private settingsFieldCreator() {
    return {
      color: this.formBuilder.control(colors.default_light.generals.accent, [
        Validators.pattern(patterns.COLOR)
      ]),
      width: this.formBuilder.control(4, [
        Validators.pattern(patterns.ONLY.NUMBERS_INT),
        Validators.max(6),
        Validators.min(1)
      ]),
      maxDescription: this.formBuilder.control(80, [
        Validators.pattern(patterns.ONLY.NUMBERS_INT),
        Validators.max(300)
      ]),
      overflowButtonText: this.formBuilder.control('see more', [
        Validators.maxLength(30),
        Validators.min(1)
      ]),
      showOverflowButton: this.formBuilder.control(true),
      withConnectionDots: this.formBuilder.control(false)
    };
  }
}

