import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-graph-container',
  templateUrl: './graph-container.component.html'
})
export class GraphContainerComponent implements OnInit, OnDestroy {

  title: string;
  routerBack: string;
  routerForward: string;
  routerBackLabel: string;
  routerForwardLabel: string;

  private routerSubs: Subscription;

  constructor(
    private router: ActivatedRoute,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.routerSubs = this.router.url.subscribe(() => {
      this.title = this.router.snapshot.data.title;
      this.router.children.map((data: { [key: string]: any }) => {
        this.title = data.data.value.title || this.title;
        this.routerBack = data.data.value.routeBack;
        this.routerForward = data.data.value.routeForward;
        this.routerBackLabel = data.data.value.routeBackLabel;
        this.routerForwardLabel = data.data.value.routeForwardLabel;
      });
    });
  }

  ngOnDestroy(): void {
    this.routerSubs.unsubscribe();
  }

  goTo(path: string): void {
    this.route.navigateByUrl(path);
  }
}
