import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { FlowChartCreator } from 'src/classes';
import { FormBuilder } from '@angular/forms';
import { downloadObjectAsJson, waitNext, clog, cerror, cwarn } from 'src/utils';
import { FlowChart } from 'src/interfaces';
import { debounceTime, map } from 'rxjs/operators';

@Component({
  selector: 'app-flow-chart-creator',
  templateUrl: './flow-chart-creator.component.html'
})
export class FlowChartCreatorComponent implements OnInit, OnDestroy, AfterViewInit {

  title: string;
  chartCreator: FlowChartCreator;
  flowChartData: FlowChart;
  showLoading: boolean;
  jsonFile: any;

  private routerSubs: Subscription;
  private formChanged: Subject<string>;

  constructor(
    private router: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    this.chartCreator = new FlowChartCreator(formBuilder);
    this.flowChartData = {
      withConnectionDots: false,
      elements: [],
      lines: []
    };
    this.formChanged = new Subject<string>();
  }

  ngOnInit(): void {
    this.routerSubs = this.router.data.subscribe((data: { [key: string]: any }) => {
      this.title = data.title;
    });
    this.chartCreator.reset.subscribe(() => {
      this.makeChart();
    });
    this.initFormChanges();
  }

  initFormChanges(): void {
    this.formChanged.pipe(
      map(() => {
        this.showLoading = true;
      }),
      debounceTime(300),
    ).subscribe(() => {
      this.showLoading = false;
      this.makeChart();
    });
  }

  resetForm(form: string) {
    this.onChangeInput();
    this.chartCreator.resetForm(form);
  }

  toggleForm() {
    this.onChangeInput();
    return this.chartCreator;
  }

  onChangeInput(event?: string) {
    this.formChanged.next(event);
  }

  ngAfterViewInit(): void {
    const container = document.querySelectorAll('.page')[0];
    container.scrollTop = 0;
  }

  ngOnDestroy(): void {
    this.routerSubs.unsubscribe();
  }

  makeChart(): void {
    this.flowChartData = this.chartCreator.buildChart(true);
  }

  downloadChart(): void {
    downloadObjectAsJson(this.chartCreator.buildChart(false));
  }

  uploadChart(event: File[]): void {
    if (event && event.length > 0) {
      const reader = new FileReader();
      reader.onload = (e) => {
        this.onReaderLoad(e);
      };
      reader.readAsText(event[0]);
    }
  }

  onReaderLoad(event): void {
    const object = JSON.parse(event.target.result);
    this.chartCreator.rebuildFromJSON(object);
    waitNext(() => {
      this.makeChart();
    });
  }
}
