import { Component, OnInit } from '@angular/core';
import { waitNext } from 'src/utils';

@Component({
  selector: 'app-diff-chart-container',
  templateUrl: './diff-chart-container.component.html'
})
export class DiffChartContainerComponent implements OnInit {

  selected: string;
  noAnimation: boolean;
  showGraph: boolean;

  constructor() {
    this.showGraph = true;
    this.noAnimation = false;
  }

  ngOnInit() {
  }

  selectChart(id: string): void {
    this.showGraph = false;
    this.selected = (this.selected === id) ? '' : id;
    this.noAnimation = true;
    setTimeout(() => {
      this.showGraph = true;
    }, 301);
  }
}
