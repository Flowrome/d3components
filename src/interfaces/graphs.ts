
export interface FlowChart {
  withConnectionDots?: boolean;
  notSingleLine?: boolean;
  lineColor?: string;
  lineWidth?: number;
  maxDescription?: number;
  overflowButtonText?: string;
  showOverflowButton?: boolean;
  lines?: ChartLine[];
  elements?: ChartElement[];
}

export interface LineChart {
  xLabel?: string;
  yLabel?: string;
  withConnectionDots?: boolean;
  withArea?: boolean;
  isZommable?: boolean;
  hasX?: boolean;
  hasY?: boolean;
  lines?: ChartLine[];
}

export interface PieChart {
  segments?: PieSegments[];
  standardColor?: string;
  showDetail?: boolean;
  fullCircle?: boolean;
}

export interface BarChart {
  bars: BarSegment[];
  xLabel?: string;
  yLabel?: string;
}

export interface ChartElement {
  id?: string;
  formId?: string;
  type: string;
  title?: string;
  description?: string;
  hasMore?: boolean;
  coords: Coords;
  innerCoords?: Coords;
}

export interface ChartLine {
  id?: string;
  formId?: string;
  name?: string;
  connections?: string;
  color?: string;
  width?: number;
  type?: string;
  coords: Coords[];
}

export interface Coords {
  formId?: string;
  x: any;
  xLabel?: string;
  y: any;
  yLabel?: string;
}

export interface PieSegments {
  value: number;
  percentage?: string;
  color?: string;
  label?: string;
  title?: string;
  description?: string;
}

export interface BarSegment {
  coords: Coords;
  name?: string;
  description?: string;
  color?: string;
}
