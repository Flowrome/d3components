import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraphContainerComponent, FlowChartCreatorComponent, DiffChartContainerComponent } from 'src/pages';
import { FlowChartComponent, LineChartComponent, InputTextComponent, PieChartComponent, BarChartComponent } from 'src/shared-components';
import { ApiService } from 'src/services';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

const pageComponents = [
  GraphContainerComponent,
  FlowChartCreatorComponent,
  DiffChartContainerComponent
];

const sharedComponents = [
  FlowChartComponent,
  LineChartComponent,
  InputTextComponent,
  PieChartComponent,
  BarChartComponent
];

const services = [
  ApiService
];


@NgModule({
  declarations: [
    AppComponent,
    ...pageComponents,
    ...sharedComponents
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    ...services
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
