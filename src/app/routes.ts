import { Routes } from '@angular/router';
import { GraphContainerComponent, FlowChartCreatorComponent, DiffChartContainerComponent } from 'src/pages';
import { FlowChartComponent, LineChartComponent, PieChartComponent, BarChartComponent } from 'src/shared-components';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    // data: {
    //   title: 'Home'
    // },
    component: GraphContainerComponent,
    children: [
      {
        path: '',
        redirectTo: 'presentation-what-is-d3',
        pathMatch: 'full'
      },
      {
        path: 'presentation',
        data: {
          flowChartPath: 'presentation',
          routeForward: 'home/presentation-what-is-d3',
          routeForwardLabel: 'What is D3.js?',
        },
        component: FlowChartComponent
      },
      {
        path: 'presentation-what-is-d3',
        data: {
          title: 'What is D3.js?',
          flowChartPath: 'presentation_gr_what_is_d3',
          routeForward: 'home/presentation-steps',
          routeForwardLabel: 'Step map',
        },
        component: FlowChartComponent
      },
      {
        path: 'presentation-steps',
        data: {
          title: 'Step map',
          flowChartPath: 'presentation_gr_steps',
          routeBack: 'home/presentation-what-is-d3',
          routeBackLabel: 'What is D3.js?',
          routeForward: 'home/presentation-canvas-vs-svg',
          routeForwardLabel: 'Canvas vs SVG',
        },
        component: FlowChartComponent
      },
      {
        path: 'presentation-canvas-vs-svg',
        data: {
          title: 'Canvas vs SVG',
          flowChartPath: 'presentation_gr_canvas_vs_svg',
          routeBack: 'home/presentation-steps',
          routeBackLabel: 'Step map',
          routeForward: 'diff-chart-masonry',
          routeForwardLabel: 'Chart examples',
        },
        component: FlowChartComponent
      },
      {
        path: 'line-chart',
        data: {
          lineChartPath: 'lineChartDates'
        },
        component: LineChartComponent
      },
      {
        path: 'pie-chart',
        data: {
          pieChartPath: 'pieChart'
        },
        component: PieChartComponent
      },
      {
        path: 'bar-chart',
        data: {
          barChartPath: 'barChart'
        },
        component: BarChartComponent
      }
    ]
  },
  {
    path: 'flow-chart-creator',
    data: {
      title: 'Flow Chart Creator'
    },
    component: FlowChartCreatorComponent
  },
  {
    path: 'diff-chart-masonry',
    data: {
      title: 'Different Charts',
      flowChartPath: 'presentation_gr_d3_hc_cjs',
      pieChartPath: 'pieChart',
      lineChartPath: 'lineChartDates',
      barChartPath: 'barChart'
    },
    component: DiffChartContainerComponent
  }
];
